# Pizzateig

## Zutaten

### Zu kaufen
* 1   kg    Mehl
* 1/4 Block Hefe

### Basis
* 625 ml    Wasser
* 40  g     Salz

## Zubereitung
* Vermengen von 600 g Mehl, Wasser, Salz und rühren (wird wie Pfannkuchenteig)
* 20 min abgedeckt stehen lassen
* ---
* Teig für 3-4 min rühren
* Restliches Mehl unterrühren
* 20 min abgedeckt stehen lassen
* ---
* Teig auf bemehlte Fläche geben und in Portionen schneiden
* Teigbällchen in Tupperdosen mind. 24 h (max. 6 Tage) in den Kühlschrank 
* ---
* Auf bemehlter Fläche / Backpapier ausrollen und belegen