# Low Carb Lebkuchen

## Zutaten

* 360 g gemahlene Haselnüsse
* 130 g gehackte Mandeln
* 250 g Erythrit
* 1 Packung Lebkuchengewürz
* 4 g Zimt
* 4 Msp. Natron
* 5 Eier
* 160 g Schokolade mit 99% Kakaoanteil

## Zubereitung

* Backofen auf 180° vorheizen
* Mandeln in der Pfanne anrösten
* Haselnüsse, Mandeln, Erythrit, Lebkuchengewürz, Zimt und Natron vermischen
* Eier hinzufügen und vermengen
* Pflaumengroße Teigballen formen und etwas plattdrücken (ca. 1 cm hoch)
* 20 min bei 180° backen
* Abkühlen lassen
* ---
* Schokolade in einer Schüssel über einem Wasserbad schmelzen
* Schokolade mit einem Pinsel auf die Lebkuchen verteilen
* Lebkuchen ruhen lassen

## Verbesserungsoptionen
* Evtl. Schokolade durch Kokosfett mit Kakao ersetzen