# Low Carb Tonkabohnen Panna Cotta

## Zutaten

### Zu kaufen
* 500 g Sahne
* 50 g Erythritzucker
* 2 Tonkabohnen
* 3,5 g Agar Agar

## Zubereitung
* Tonkabohnen raspeln (Körnung wie Muskat) und mit dem Erythrit in die Sahne geben
* Aufkochen und Agar Agar mit Schneebesen unterrühren
* Nochmals aufkochen und in Gläser o.ä. abfüllen
* 1 Stunde in den Kühlschrank stellen
