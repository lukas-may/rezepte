# Low Carb Bauernbrot

## Zutaten für ein kleines Brot

### Zu kaufen
* 200 ml Joghurt
* 3 Eier
* 33 g Goldleinsamenmehl entölt
* 33 g Hanfmehl
* 33 g Weizeneiweiß
* 50 g Kartoffelfasern
* 20 g Flohsamenschalen
* 10 g Backpulver
* 2 TL Brotgewürz


### Basis
* 1 TL Salz (eher weniger)
* 1 TL Senf mittelscharf
* 25 ml weißer Balsamico

## Zubereitung
* Alle Zutaten zusammenrühren
* Kreuzweise einschneiden und mit etwas Kartoffelfasern bestreuen
* 5 min quellen lassen
* Bei 180° 60 min backen