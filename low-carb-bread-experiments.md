# Low Carb Experimente


## (1) Mit Mehlen und Eiern aber ohne Molkeprodukte

### Inspiration / Ähnliche Rezepte
* https://www.chefkoch.de/rezepte/3316941492622177/Low-Carb-Brot-mein-Bestes.html

### Zutaten
* 100g Mandelmehl
* 50g Hanfmehl
* 50g Goldleinsamen gemahlen (Nicht mega fein)
* 50g Flosamenschalen
* Eine kleine Hand Voll Walnüsse 
* Ein wenig Salz :D 
* 15g Backpulver
* 1 TL Brotgewürz
* 3 Eier
* 1 Eiweiß
* 35ml Apfelessig
* 250ml kochendes Wasser
* Kümmel / Salz Kruste

Backzeit: 70 Minuten, letzte 10 Minuten umdrehen 

### Learnings
* Hahahah voll der Ball geworden - ging krank auf
* mega lecker, überhaupt nicht sandig
* bisschen mehr Salz
* Kümmel, Salz Kruste nächstes Mal mehr
* ruhig mehr Walnüsse und das nächste Mal auch Kastanien rein 
* aber an sich sehr zufrieden
* Nach einem Tag auch noch sehr lecker, wenig bis nicht sandig
* beim nächsten Mal Teig besser mischen da Luftblasen unterschiedlich groß

## (2) Mega Random Schokoball-Experiment

### Zutaten
* 70g Mandelmehl
* Xg Kokosmehl (vielleicht so 40g :D)
* 60g Butter
* 50g Xucker
* Bissl Backpulver
* Bissl Vanillezucker
* 2 Eier

### Learnings
* vllt. so 30 Minuten Backzeit eher
* eher flache Kekse
* Mehr Rezepte benchmarken
* eher in Richtung Trockengebäck gehen

## (3) Kürbiskernmehl als Hauptmehl

### Zutaten
* 100g Kürbiskernmehl
* 50g Hanfmehl
* 50g Mandelmehl
* 50g Flosamenschalen
* Eine kleine Hand Voll Walnüsse 
* Ein wenig Salz :D 
* 15g Backpulver
* 6g Brotgewürz
* 3 Eier
* 1 Eiweiß
* 35ml Apfelessig
* 250ml kochendes Wasser
* Kümmel / Salz Kruste

## Ideen für nächste Tests
* https://lecker-low-carb.de/low-carb-rezepte/leben-ohne-teig-geht-nicht-%E2%80%93-teig-ohne-carb-schon/low-carb-rezepte-brot/bilderstrecke-brot-backen.html