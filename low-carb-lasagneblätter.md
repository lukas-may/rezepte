# Low carb Lasagneblätter

## Zutaten (2 Platten für Auflaufform ~20x35)

* 250 g Quark
* 3     Eier
* 100 g geriebener Käse

## Zubereitung

* Käse mit Messer noch etwas zerkleinern
* Zutaten vermengen und quirlen
* Auf Backpapier verstreichen und bei 180° 20 min backen (bis diese braun geworden sind)
* Etwas warten und dann vorsichtig vom Backpapier ablösen