# Low Carb Apfelkuchen

## Zutaten für eine kleine Springform (25 cm oder so?!)

### Zu kaufen
* 180g Mandelmehl
* 75g gemahlene Haselnüsse
* 150g Frischkäse
* 3 Tonkabohnen
* 3 TL Zimt
* 6 Eier
* 4 große Äpfel
* 120g Erythrit
* 170g Butter

### Basis
* 2 TL Backpulver
* 1 TL Salz

## Zubereitung
* Backofen auf 180° vorheizen
* Äpfel schälen und kleinschneiden
* Tonkabohnen reiben und mit der Butter in einen kleinen Topf zum schmelzen geben
* Eier, Erythrit, Butter und Frischkäse verquirlen
* Mandelmehl, gemahlene Haselnüsse, Zimt, Backpulver und Salz zugeben und verquirlen
* Äpfel unterheben
* 35-40 Minuten backen