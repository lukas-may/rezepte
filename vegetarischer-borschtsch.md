# Vegetarischer Borschtsch

## Zutaten

### Zu kaufen
* 500 g Rote Beete (roh)
* 1 Stange Lauch
* 500 g Weiskohl
* 3 Karotten
* 500 g Kartoffeln (für Low Carb anstelle 500 g Karotten / Blumenkohl)
* 1 Gemüsezwiebel
* 1 Becher Schmand
* Dill (frisch oder getrocknet)

### Basis
* Gemüsebrühe
* Öl
* weißer Balsamicoessig

## Zubereitung
* Andünsten in Öl von
  * gehackten Zwiebeln
  * in Stiften geschnittener Rote Beete
  * in Streifen geschnittenem Weiskohl
  * gewürfelten Kartoffeln / zerkleinertem Blumenkohl
* Nach 15 min mit 1,5 l Gemüsebrühe ablöschen
* Karotten (in Scheiben) und Lauch (in Ringen) zugeben und 15 min köcheln lassen
* Abschmecken mit weißem Balsamicoessig, Salz und Pfeffer
* ---
* Mit Dill und Schmand servieren