# Low Carb Pizzaboden

## Zutaten (3 Teige)

### Zu kaufen
* 30  g Chia
* 200 g Leinsamen
* 50  g Hanfmehl
* 125 g Kokosmehl

### Basis
* 250 ml Wasser

## Zubereitung

* In einer Schüssel
  * Chiasamen mit Wasser vermischen
  * 15 min warten
  * Restliche Zutaten hinzugeben und vermengen
* Ausrollen und vorbacken

## Verbesserungspotential
* Evtl. Teil der Leinsamen durch Hanfmehl / Kokosmehl ersetzen
* Mit Kartoffelfasern experimentieren